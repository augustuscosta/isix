# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160320132018) do

  create_table "adesaos", force: :cascade do |t|
    t.datetime "data"
    t.float    "valor"
    t.datetime "validade"
    t.integer  "usuario_id"
    t.integer  "tipo_usuario_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "anuncios", force: :cascade do |t|
    t.string   "titulo"
    t.string   "subtitulo"
    t.float    "valor"
    t.string   "descricao"
    t.integer  "categoria_anuncio_id"
    t.integer  "endereco_id"
    t.integer  "forma_pagamento_id"
    t.integer  "usuario_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "cached_votes_up",      default: 0
  end

  add_index "anuncios", ["cached_votes_up"], name: "index_anuncios_on_cached_votes_up"

  create_table "banners", force: :cascade do |t|
    t.boolean  "geral"
    t.integer  "pai_id"
    t.integer  "estado_id"
    t.integer  "cidade_id"
    t.text     "retina_dimensions"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "carreiras", force: :cascade do |t|
    t.boolean  "nivel1"
    t.boolean  "nivel2"
    t.boolean  "nivel3"
    t.boolean  "nivel4"
    t.boolean  "nivel5"
    t.boolean  "nivel6"
    t.boolean  "nivel7"
    t.boolean  "nivel8"
    t.boolean  "nivel9"
    t.boolean  "nivel10"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categoria_anuncios", force: :cascade do |t|
    t.string   "nome"
    t.integer  "parent_id"
    t.text     "retina_dimensions"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "categoria_cupoms", force: :cascade do |t|
    t.string   "nome"
    t.integer  "parent_id"
    t.text     "retina_dimensions"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "cidades", force: :cascade do |t|
    t.string   "nome"
    t.integer  "estado_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comentarios", force: :cascade do |t|
    t.text     "corpo"
    t.integer  "negocio_id"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comentarios", ["negocio_id"], name: "index_comentarios_on_negocio_id"
  add_index "comentarios", ["usuario_id"], name: "index_comentarios_on_usuario_id"

  create_table "comments", force: :cascade do |t|
    t.string   "title",            limit: 50, default: ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "usuario_id"
    t.string   "role",                        default: "comments"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["commentable_id"], name: "index_comments_on_commentable_id"
  add_index "comments", ["commentable_type"], name: "index_comments_on_commentable_type"
  add_index "comments", ["usuario_id"], name: "index_comments_on_usuario_id"

  create_table "conta", force: :cascade do |t|
    t.integer  "banco"
    t.string   "nome"
    t.string   "cpf_cnpj"
    t.string   "agencia"
    t.string   "conta"
    t.string   "operacao"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cupoms", force: :cascade do |t|
    t.string   "titulo"
    t.string   "subtitulo"
    t.float    "valor"
    t.float    "valor_desconto"
    t.string   "descricao"
    t.integer  "quantidade_total"
    t.integer  "quantidade_pessoa"
    t.integer  "quantidade_dia"
    t.string   "regras"
    t.date     "utilizacao_inicio"
    t.date     "utilizacao_fim"
    t.date     "exibicao_inicio"
    t.date     "exibicao_fim"
    t.integer  "hora_consumo_inicio"
    t.integer  "hora_consumo_fim"
    t.boolean  "domingo"
    t.boolean  "segunda"
    t.boolean  "terca"
    t.boolean  "quarta"
    t.boolean  "quinta"
    t.boolean  "sexta"
    t.boolean  "sabado"
    t.integer  "categoria_cupom_id"
    t.integer  "usuario_id"
    t.integer  "forma_pagamento_id"
    t.integer  "endereco_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "cached_votes_up",     default: 0
  end

  add_index "cupoms", ["cached_votes_up"], name: "index_cupoms_on_cached_votes_up"

  create_table "curtida_anuncios", force: :cascade do |t|
    t.integer  "anuncio_id"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "descontos", force: :cascade do |t|
    t.integer  "cupom_id"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "enderecos", force: :cascade do |t|
    t.string   "bairro"
    t.string   "logradouro"
    t.string   "complemento"
    t.string   "cep"
    t.integer  "pais_id"
    t.integer  "estado_id"
    t.integer  "cidade_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "estados", force: :cascade do |t|
    t.string   "nome"
    t.string   "sigla"
    t.integer  "pai_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "follows", force: :cascade do |t|
    t.integer  "followable_id",                   null: false
    t.string   "followable_type",                 null: false
    t.integer  "follower_id",                     null: false
    t.string   "follower_type",                   null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables"
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows"

  create_table "forma_pagamentos", force: :cascade do |t|
    t.string   "nome"
    t.text     "retina_dimensions"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "grupos", force: :cascade do |t|
    t.string   "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "horario_funcionamentos", force: :cascade do |t|
    t.integer  "negocio_id"
    t.string   "segunda_abertura"
    t.string   "segunda_fechamento"
    t.string   "terca_abertura"
    t.string   "terca_fechamento"
    t.string   "quarta_abertura"
    t.string   "quarta_fechamento"
    t.string   "quinta_abertura"
    t.string   "quinta_fechamento"
    t.string   "sexta_abertura"
    t.string   "sexta_fechamento"
    t.string   "sabado_abertura"
    t.string   "sabado_fechamento"
    t.string   "domingo_abertura"
    t.string   "domingo_fechamento"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "horario_funcionamentos", ["negocio_id"], name: "index_horario_funcionamentos_on_negocio_id"

  create_table "logs", force: :cascade do |t|
    t.string   "acao"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mensagem_anuncios", force: :cascade do |t|
    t.string   "mensagem"
    t.integer  "anuncio_id"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mensagem_cupoms", force: :cascade do |t|
    t.string   "mensagem"
    t.integer  "cupom_id"
    t.integer  "usuario_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mensagems", force: :cascade do |t|
    t.string   "mensagem"
    t.datetime "data"
    t.integer  "remetente_id"
    t.integer  "destinatario_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "negocios", force: :cascade do |t|
    t.string   "nome"
    t.string   "site"
    t.string   "telefone"
    t.string   "whatsapp"
    t.string   "celular"
    t.string   "facebook"
    t.string   "instagram"
    t.string   "googleplus"
    t.string   "titulo_descricao"
    t.text     "descricao"
    t.integer  "abertura"
    t.integer  "fechamento"
    t.integer  "usuario_id"
    t.text     "html"
    t.text     "retina_dimensions"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "pais", force: :cascade do |t|
    t.string   "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "retiradas", force: :cascade do |t|
    t.float    "valor"
    t.datetime "data"
    t.integer  "usuario_id"
    t.integer  "conta_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_usuarios", force: :cascade do |t|
    t.string   "nome"
    t.float    "valor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "usuarios", force: :cascade do |t|
    t.string   "nome"
    t.string   "usuario"
    t.string   "email",                  default: "", null: false
    t.string   "password"
    t.string   "telefone"
    t.string   "sobre"
    t.boolean  "bloqueado"
    t.text     "mensagem_bloqueio"
    t.integer  "endereco_id"
    t.integer  "tipo_usuario_id"
    t.integer  "grupo_id"
    t.integer  "parent_id"
    t.float    "saldo"
    t.text     "retina_dimensions"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "provider"
    t.string   "uid"
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true
  add_index "usuarios", ["reset_password_token"], name: "index_usuarios_on_reset_password_token", unique: true

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"

end
