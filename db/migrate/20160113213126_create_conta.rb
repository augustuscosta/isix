class CreateConta < ActiveRecord::Migration
  def change
    create_table :conta do |t|
      t.integer :banco
      t.string :nome
      t.string :cpf_cnpj
      t.string :agencia
      t.string :conta
      t.string :operacao
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
