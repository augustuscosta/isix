class AddCashedVotesToAnuncios < ActiveRecord::Migration
  def change
    add_column :anuncios, :cached_votes_up, :integer, :default => 0
    add_index  :anuncios, :cached_votes_up
  end
end
