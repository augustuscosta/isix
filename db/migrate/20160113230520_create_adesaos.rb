class CreateAdesaos < ActiveRecord::Migration
  def change
    create_table :adesaos do |t|
      t.datetime :data
      t.float :valor
      t.datetime :validade
      t.integer :usuario_id
      t.integer :tipo_usuario_id

      t.timestamps null: false
    end
  end
end
