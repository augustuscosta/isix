class CreateFormaPagamentos < ActiveRecord::Migration
  def change
    create_table :forma_pagamentos do |t|
      t.string :nome
      t.text :retina_dimensions
      t.attachment :imagem

      t.timestamps null: false
    end
  end
end
