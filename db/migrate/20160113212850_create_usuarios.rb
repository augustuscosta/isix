class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nome
      t.string :usuario
      t.string :email,              null: false, default: ""
      t.string :password
      t.string :telefone
      t.string :sobre
      t.boolean :bloqueado
      t.text :mensagem_bloqueio
      t.integer :endereco_id
      t.integer :tipo_usuario_id
      t.integer :grupo_id
      t.integer :parent_id
      t.float :saldo
      t.text :retina_dimensions
      t.attachment :imagem

      t.timestamps null: false
    end
  end
end
