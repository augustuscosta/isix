class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :acao
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
