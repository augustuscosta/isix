class CreateMensagems < ActiveRecord::Migration
  def change
    create_table :mensagems do |t|
      t.string :mensagem
      t.datetime :data
      t.integer :remetente_id
      t.integer :destinatario_id

      t.timestamps null: false
    end
  end
end
