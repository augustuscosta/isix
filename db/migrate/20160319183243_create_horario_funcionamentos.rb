class CreateHorarioFuncionamentos < ActiveRecord::Migration
  def change
    create_table :horario_funcionamentos do |t|
      t.integer :negocio_id
      t.string :segunda_abertura
      t.string :segunda_fechamento
      t.string :terca_abertura
      t.string :terca_fechamento
      t.string :quarta_abertura
      t.string :quarta_fechamento
      t.string :quinta_abertura
      t.string :quinta_fechamento
      t.string :sexta_abertura
      t.string :sexta_fechamento
      t.string :sabado_abertura
      t.string :sabado_fechamento
      t.string :domingo_abertura
      t.string :domingo_fechamento

      t.timestamps null: false
    end
    add_index :horario_funcionamentos, :negocio_id
  end
end
