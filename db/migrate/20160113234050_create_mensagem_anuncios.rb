class CreateMensagemAnuncios < ActiveRecord::Migration
  def change
    create_table :mensagem_anuncios do |t|
      t.string :mensagem
      t.integer :anuncio_id
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
