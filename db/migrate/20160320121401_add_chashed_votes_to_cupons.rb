class AddChashedVotesToCupons < ActiveRecord::Migration
  def change
    add_column :cupoms, :cached_votes_up, :integer, :default => 0
    add_index  :cupoms, :cached_votes_up
  end
end
