class CreateCategoriaAnuncios < ActiveRecord::Migration
  def change
    create_table :categoria_anuncios do |t|
      t.string :nome
      t.integer :parent_id
      t.text :retina_dimensions
      t.attachment :imagem

      t.timestamps null: false
    end
  end
end
