class CreateNegocios < ActiveRecord::Migration
  def change
    create_table :negocios do |t|
      t.string :nome
      t.string :site
      t.string :telefone
      t.string :whatsapp
      t.string :celular
      t.string :facebook
      t.string :instagram
      t.string :googleplus
      t.string :titulo_descricao
      t.text :descricao
      t.integer :abertura
      t.integer :fechamento
      t.integer :usuario_id
      t.text :html
      t.text :retina_dimensions
      t.attachment :imagem

      t.timestamps null: false
    end
  end
end
