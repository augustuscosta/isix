class CreateRetiradas < ActiveRecord::Migration
  def change
    create_table :retiradas do |t|
      t.float :valor
      t.datetime :data
      t.integer :usuario_id
      t.integer :conta_id

      t.timestamps null: false
    end
  end
end
