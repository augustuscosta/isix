class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.boolean :geral
      t.integer :pai_id
      t.integer :estado_id
      t.integer :cidade_id
      t.text :retina_dimensions
      t.attachment :imagem
      t.timestamps null: false
    end
  end
end
