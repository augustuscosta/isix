class CreateTipoUsuarios < ActiveRecord::Migration
  def change
    create_table :tipo_usuarios do |t|
      t.string :nome
      t.float :valor

      t.timestamps null: false
    end
  end
end
