class CreateAnuncios < ActiveRecord::Migration
  def change
    create_table :anuncios do |t|
      t.string :titulo
      t.string :subtitulo
      t.float :valor
      t.string :descricao
      t.integer :categoria_anuncio_id
      t.integer :endereco_id
      t.integer :forma_pagamento_id
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
