class CreateMensagemCupoms < ActiveRecord::Migration
  def change
    create_table :mensagem_cupoms do |t|
      t.string :mensagem
      t.integer :cupom_id
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
