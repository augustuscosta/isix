class CreateCupoms < ActiveRecord::Migration
  def change
    create_table :cupoms do |t|
      t.string :titulo
      t.string :subtitulo
      t.float :valor
      t.float :valor_desconto
      t.string :descricao
      t.integer :quantidade_total
      t.integer :quantidade_pessoa
      t.integer :quantidade_dia
      t.string :regras
      t.date :utilizacao_inicio
      t.date :utilizacao_fim
      t.date :exibicao_inicio
      t.date :exibicao_fim
      t.integer :hora_consumo_inicio
      t.integer :hora_consumo_fim
      t.boolean :domingo
      t.boolean :segunda
      t.boolean :terca
      t.boolean :quarta
      t.boolean :quinta
      t.boolean :sexta
      t.boolean :sabado
      t.integer :categoria_cupom_id
      t.integer :usuario_id
      t.integer :forma_pagamento_id
      t.integer :forma_pagamento_id
      t.integer :endereco_id

      t.timestamps null: false
    end
  end
end
