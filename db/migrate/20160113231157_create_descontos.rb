class CreateDescontos < ActiveRecord::Migration
  def change
    create_table :descontos do |t|
      t.integer :cupom_id
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
