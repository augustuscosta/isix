class CreateCarreiras < ActiveRecord::Migration
  def change
    create_table :carreiras do |t|
      t.boolean :nivel1
      t.boolean :nivel2
      t.boolean :nivel3
      t.boolean :nivel4
      t.boolean :nivel5
      t.boolean :nivel6
      t.boolean :nivel7
      t.boolean :nivel8
      t.boolean :nivel9
      t.boolean :nivel10
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
