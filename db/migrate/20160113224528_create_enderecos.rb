class CreateEnderecos < ActiveRecord::Migration
  def change
    create_table :enderecos do |t|
      t.string :bairro
      t.string :logradouro
      t.string :complemento
      t.string :cep
      t.integer :pais_id
      t.integer :estado_id
      t.integer :cidade_id

      t.timestamps null: false
    end
  end
end
