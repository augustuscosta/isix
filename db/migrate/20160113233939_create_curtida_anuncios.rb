class CreateCurtidaAnuncios < ActiveRecord::Migration
  def change
    create_table :curtida_anuncios do |t|
      t.integer :anuncio_id
      t.integer :usuario_id

      t.timestamps null: false
    end
  end
end
