class CreateCategoriaCupoms < ActiveRecord::Migration
  def change
    create_table :categoria_cupoms do |t|
      t.string :nome
      t.integer :parent_id
      t.text :retina_dimensions
      t.attachment :imagem

      t.timestamps null: false
    end
  end
end
