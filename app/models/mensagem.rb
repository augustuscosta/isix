class Mensagem < ActiveRecord::Base
  belongs_to :remetente, :foreign_key => "remetente_id", :class_name => "Usuario"
  belongs_to :destinatario, :foreign_key => "destinatario_id", :class_name => "Usuario"
end
