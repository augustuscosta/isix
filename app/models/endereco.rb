class Endereco < ActiveRecord::Base
  belongs_to :pais
  belongs_to :estado
  belongs_to :cidade
  has_many :cupoms
  has_many :anuncios
  has_many :usuarios
end
