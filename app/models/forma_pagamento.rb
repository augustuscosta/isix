class FormaPagamento < ActiveRecord::Base
  has_many :anuncios
  has_many :cupoms

  retina!
  has_attached_file :imagem,
                    :styles => { :medium => "300x300#", :thumb => "100x100#"},
                    :retina => { :quality => 100 }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :imagem,
                                    :content_type => ["image/jpg", "image/jpeg", "image/png"],
                                    message: "Formato inválido!"
                                    
end
