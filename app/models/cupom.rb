class Cupom < ActiveRecord::Base
  belongs_to :categoria_cupom
  belongs_to :usuario
  belongs_to :forma_pagamento
  belongs_to :endereco
  has_many :mensagem_cupoms
  has_many :curtida_cupoms
  has_many :descontos

  acts_as_votable
end
