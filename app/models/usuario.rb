class Usuario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable#, :omniauth_providers => [:facebook]

  belongs_to :endereco
  belongs_to :tipo_usuario
  belongs_to :grupo
  belongs_to :parent, :foreign_key => "parent_id"
  has_many :usuarios, :foreign_key => "parent_id"
  has_many :retiradas
  has_many :mensagens_enviadas, :foreign_key => "remetente_id", :class_name => "Mensagem"
  has_many :mensagens_recebidas, :foreign_key => "destinatario_id", :class_name => "Mensagem"
  has_one :negocio
  has_many :mensagem_cupoms
  has_many :mensagem_anuncios
  has_many :logs
  has_many :descontos
  has_many :curtida_cupoms
  has_many :curtida_anuncios
  has_many :conta
  has_one :carreira
  has_many :cupoms
  has_many :anuncios

  acts_as_voter
  acts_as_follower
  acts_as_followable


  retina!
  has_attached_file :imagem,
                    :styles => { :medium => "300x300#", :thumb => "100x100#"},
                    :retina => { :quality => 100 }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :imagem,
                                    :content_type => ["image/jpg", "image/jpeg", "image/png"],
                                    message: "Formato inválido!"


  def is_admin
    if tipo_usuario == nil
      return false
    end
    if tipo_usuario.id == 4
      return true
    end
    false
  end

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_initialize.tap do |usuario|
      usuario.provider = auth.provider
      usuario.uid = auth.uid
      usuario.nome = auth.info.name
      usuario.email = auth.info.email
      #usuario.password = Devise.friendly_token[0,20]
      usuario.imagem_file_name = auth.info.image # assuming the user model has an image
      usuario.save!
    end  
  end

  def self.new_with_session(params, session)
    if session["devise.user_attributes"]
      new(session["devise.user_attributes"], without_protrection: true) do |usuario|
        usuario.attributes = params
        usuario.valid?
      end
    else
      super
    end
  end

  def password_required?
    super && provider.blank?
  end

  def update_with_password(params, *options)
    if encrypted.password.blank?
      updade_attributes(params, *options)
    else
      super
    end
  end

end
