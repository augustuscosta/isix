class Negocio < ActiveRecord::Base
  belongs_to :usuario

  has_many :comentarios
  has_one :horario_funcionamento

  acts_as_commentable
end
