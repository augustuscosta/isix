class Retirada < ActiveRecord::Base
  belongs_to :usuario
  belongs_to :contum
  belongs_to :usuario

  def self.search(query, realizado)
    joins(:usuario).joins(:contum).where("usuarios.nome like ? OR
                           usuarios.usuario like ? OR
                           usuarios.email like ? OR
                           usuarios.telefone like ? OR
                           usuarios.sobre like ? OR
                           conta.conta like ? OR
                           conta.agencia like ? OR
                           conta.nome like ? OR
                           conta.cpf_cnpj like ?"
                           , "%#{query}%"
                           , "%#{query}%"
                           , "%#{query}%"
                           , "%#{query}%"
                           , "%#{query}%"
                           , "%#{query}%"
                           , "%#{query}%"
                           , "%#{query}%"
                           , "%#{query}%").where(realizado: realizado)
  end

  def self.search_between(query, inicio, fim)
    where(created_at: inicio..fim).joins(:usuario).where("usuarios.nome like ? OR acao like ?", "%#{query}%", "%#{query}%").limit(50).order("created_at DESC")
  end

end
