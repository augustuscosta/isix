class HorarioFuncionamento < ActiveRecord::Base
  belongs_to :negocio

  def to_list
    horarios = []

    horarios << "DOM #{domingo_abertura} a #{domingo_fechamento}" if !self.domingo_abertura.blank? && !self.domingo_fechamento.blank?
    horarios << "SEG #{segunda_abertura} a #{segunda_abertura}" if !self.segunda_abertura.blank? && !self.segunda_abertura.blank?
    horarios << "TER #{terca_abertura} a #{terca_fechamento}" if !self.terca_abertura.blank? && !self.terca_fechamento.blank?
    horarios << "QUA #{quarta_abertura} a #{quarta_fechamento}" if !self.quarta_abertura.blank? && !self.quarta_fechamento.blank?
    horarios << "QUI #{quinta_abertura} a #{quinta_fechamento}" if !self.quinta_abertura.blank? && !self.quinta_fechamento.blank?
    horarios << "SEX #{sexta_abertura} a #{sexta_fechamento}" if !self.sexta_abertura.blank? && !self.sexta_fechamento.blank?
    horarios << "SAB #{sabado_abertura} a #{sabado_fechamento}" if !self.sabado_abertura.blank? && !self.sabado_fechamento.blank?

    horarios
  end
end
