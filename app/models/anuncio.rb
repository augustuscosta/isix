class Anuncio < ActiveRecord::Base
  belongs_to :categoria_anuncio
  belongs_to :endereco
  belongs_to :forma_pagamento
  belongs_to :usuario
  has_many :curtida_anuncios

  acts_as_votable
end
