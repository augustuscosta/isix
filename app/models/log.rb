class Log < ActiveRecord::Base
  belongs_to :usuario

  def self.search(query)
    joins(:usuario).where("usuarios.nome like ? OR acao like ?", "%#{query}%", "%#{query}%").limit(50).order("created_at DESC")
  end

  def self.search_between(query, inicio, fim)
    where(created_at: inicio..fim).joins(:usuario).where("usuarios.nome like ? OR acao like ?", "%#{query}%", "%#{query}%").limit(50).order("created_at DESC")
  end

end
