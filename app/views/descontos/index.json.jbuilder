json.array!(@descontos) do |desconto|
  json.extract! desconto, :id, :cupom_id, :usuario_id
  json.url desconto_url(desconto, format: :json)
end
