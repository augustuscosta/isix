json.array!(@negocios) do |negocio|
  json.extract! negocio, :id, :nome, :site, :telefone, :whatsapp, :facebook, :instagram, :googleplus, :descricao, :abertura, :fechamento, :usuario_id
  json.url negocio_url(negocio, format: :json)
end
