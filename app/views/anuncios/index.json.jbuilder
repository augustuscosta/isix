json.array!(@anuncios) do |anuncio|
  json.extract! anuncio, :id, :titulo, :subtitulo, :valor, :descricao, :categoria_anuncio_id, :endereco_id, :forma_pagamento_id, :usuario_id
  json.url anuncio_url(anuncio, format: :json)
end
