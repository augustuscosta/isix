json.array!(@banners) do |banner|
  json.extract! banner, :id, :geral, :pais_id, :estado_id, :cidade_id
  json.url banner_url(banner, format: :json)
end
