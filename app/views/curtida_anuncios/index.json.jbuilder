json.array!(@curtida_anuncios) do |curtida_anuncio|
  json.extract! curtida_anuncio, :id, :anuncio_id, :usuario_id
  json.url curtida_anuncio_url(curtida_anuncio, format: :json)
end
