json.array!(@conta) do |contum|
  json.extract! contum, :id, :banco, :nome, :cpf_cnpj, :agencia, :conta, :operacao, :usuario_id
  json.url contum_url(contum, format: :json)
end
