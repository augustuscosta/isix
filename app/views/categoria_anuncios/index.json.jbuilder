json.array!(@categoria_anuncios) do |categoria_anuncio|
  json.extract! categoria_anuncio, :id, :nome, :parent_id
  json.url categoria_anuncio_url(categoria_anuncio, format: :json)
end
