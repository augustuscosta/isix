json.array!(@mensagem_cupoms) do |mensagem_cupom|
  json.extract! mensagem_cupom, :id, :mensagem, :cupom_id, :usuario_id
  json.url mensagem_cupom_url(mensagem_cupom, format: :json)
end
