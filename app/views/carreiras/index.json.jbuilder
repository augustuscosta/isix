json.array!(@carreiras) do |carreira|
  json.extract! carreira, :id, :nivel1, :nivel2, :nivel3, :nivel4, :nivel5, :nivel6, :nivel7, :nivel8, :nivel9, :nivel10, :usuario_id
  json.url carreira_url(carreira, format: :json)
end
