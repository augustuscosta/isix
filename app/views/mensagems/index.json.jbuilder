json.array!(@mensagems) do |mensagem|
  json.extract! mensagem, :id, :mensagem, :data, :remetente_id, :destinatario_id
  json.url mensagem_url(mensagem, format: :json)
end
