json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :nome, :usuario, :email, :password, :telefone, :sobre, :bloqueado, :mensagem_bloqueio, :endereco_id, :tipo_usuario_id, :grupo_id, :parent_id
  json.url usuario_url(usuario, format: :json)
end
