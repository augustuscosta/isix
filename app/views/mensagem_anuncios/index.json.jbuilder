json.array!(@mensagem_anuncios) do |mensagem_anuncio|
  json.extract! mensagem_anuncio, :id, :mensagem, :anuncio_id, :usuario_id
  json.url mensagem_anuncio_url(mensagem_anuncio, format: :json)
end
