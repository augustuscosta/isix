json.array!(@retiradas) do |retirada|
  json.extract! retirada, :id, :valor, :data, :usuario_id, :conta_id
  json.url retirada_url(retirada, format: :json)
end
