json.array!(@enderecos) do |endereco|
  json.extract! endereco, :id, :bairro, :logradouro, :complemento, :cep, :pais_id, :estado_id, :cidade_id
  json.url endereco_url(endereco, format: :json)
end
