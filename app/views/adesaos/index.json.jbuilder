json.array!(@adesaos) do |adesao|
  json.extract! adesao, :id, :data, :valor, :validade, :usuario_id, :tipo_usuario_id
  json.url adesao_url(adesao, format: :json)
end
