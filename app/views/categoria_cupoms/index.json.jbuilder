json.array!(@categoria_cupoms) do |categoria_cupom|
  json.extract! categoria_cupom, :id, :nome, :parent_id
  json.url categoria_cupom_url(categoria_cupom, format: :json)
end
