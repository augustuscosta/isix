class MensagemCupomsController < ApplicationController
  before_action :set_mensagem_cupom, only: [:show, :edit, :update, :destroy]

  # GET /mensagem_cupoms
  # GET /mensagem_cupoms.json
  def index
    @mensagem_cupoms = MensagemCupom.all
  end

  # GET /mensagem_cupoms/1
  # GET /mensagem_cupoms/1.json
  def show
  end

  # GET /mensagem_cupoms/new
  def new
    @mensagem_cupom = MensagemCupom.new
  end

  # GET /mensagem_cupoms/1/edit
  def edit
  end

  # POST /mensagem_cupoms
  # POST /mensagem_cupoms.json
  def create
    @mensagem_cupom = MensagemCupom.new(mensagem_cupom_params)

    respond_to do |format|
      if @mensagem_cupom.save
        format.html { redirect_to @mensagem_cupom, notice: 'Mensagem cupom was successfully created.' }
        format.json { render :show, status: :created, location: @mensagem_cupom }
      else
        format.html { render :new }
        format.json { render json: @mensagem_cupom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mensagem_cupoms/1
  # PATCH/PUT /mensagem_cupoms/1.json
  def update
    respond_to do |format|
      if @mensagem_cupom.update(mensagem_cupom_params)
        format.html { redirect_to @mensagem_cupom, notice: 'Mensagem cupom was successfully updated.' }
        format.json { render :show, status: :ok, location: @mensagem_cupom }
      else
        format.html { render :edit }
        format.json { render json: @mensagem_cupom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mensagem_cupoms/1
  # DELETE /mensagem_cupoms/1.json
  def destroy
    @mensagem_cupom.destroy
    respond_to do |format|
      format.html { redirect_to mensagem_cupoms_url, notice: 'Mensagem cupom was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mensagem_cupom
      @mensagem_cupom = MensagemCupom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mensagem_cupom_params
      params.require(:mensagem_cupom).permit(:mensagem, :cupom_id, :usuario_id)
    end
end
