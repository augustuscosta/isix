class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :is_admin?, :is_marketing?, :is_financeiro?, :is_admin_master?

  def create_log(acao)
    if usuario_signed_in?
      Log.create(acao: acao, usuario_id: current_usuario.id)
    else
      Log.create(acao: acao, usuario_id: 0)
    end
  end

  def is_admin?
    if usuario_signed_in?
      return current_usuario.is_admin
    end
    return false
  end

  def is_marketing
    unless is_marketing?
      permission_denied
    end
  end

  def is_marketing?
    if is_admin?
      if current_usuario.grupo_id == 2 || current_usuario.grupo_id == 1
        return true
      end
    end
    false
  end

  def is_financeiro
    unless is_financeiro?
      permission_denied
    end
  end


  def is_financeiro?
    if is_admin?
      if current_usuario.grupo_id == 3 || current_usuario.grupo_id == 1
        return true
      end
    end
    false
  end

  def is_admin_master
    unless is_admin_master?
      permission_denied
    end
  end

  def is_admin_master?
    if is_admin?
      if current_usuario.grupo_id == 1
        return true
      end
    end
    false
  end

  def permission_denied
    render :file => "public/401.html", :status => :unauthorized
  end

end
