class CategoriaAnunciosController < ApplicationController
  before_action :set_categoria_anuncio, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!, except: [:index, :show]
  before_action :is_marketing, except: [:index, :show]

  # GET /categoria_anuncios
  # GET /categoria_anuncios.json
  def index
    @categoria_anuncios = CategoriaAnuncio.all
    create_log("Listar categorias de anuncio")
  end

  # GET /categoria_anuncios/1
  # GET /categoria_anuncios/1.json
  def show
    create_log("Visualizar categoria de anuncio")
  end

  # GET /categoria_anuncios/new
  def new
    @categoria_anuncio = CategoriaAnuncio.new
    create_log("Formulário de criação de nova categoria de anuncio")
  end

  # GET /categoria_anuncios/1/edit
  def edit
    create_log("Formulário de edição categoria de anuncio")
  end

  # POST /categoria_anuncios
  # POST /categoria_anuncios.json
  def create
    @categoria_anuncio = CategoriaAnuncio.new(categoria_anuncio_params)

    respond_to do |format|
      if @categoria_anuncio.save
        format.html { redirect_to @categoria_anuncio, notice: 'Categoria anuncio was successfully created.' }
        format.json { render :show, status: :created, location: @categoria_anuncio }
      else
        format.html { render :new }
        format.json { render json: @categoria_anuncio.errors, status: :unprocessable_entity }
      end
    end
    create_log("Criou nova categoria de anuncio")
  end

  # PATCH/PUT /categoria_anuncios/1
  # PATCH/PUT /categoria_anuncios/1.json
  def update
    respond_to do |format|
      if @categoria_anuncio.update(categoria_anuncio_params)
        format.html { redirect_to @categoria_anuncio, notice: 'Categoria anuncio was successfully updated.' }
        format.json { render :show, status: :ok, location: @categoria_anuncio }
      else
        format.html { render :edit }
        format.json { render json: @categoria_anuncio.errors, status: :unprocessable_entity }
      end
    end
    create_log("Editou categoria de anuncio")
  end

  # DELETE /categoria_anuncios/1
  # DELETE /categoria_anuncios/1.json
  def destroy
    @categoria_anuncio.destroy
    respond_to do |format|
      format.html { redirect_to categoria_anuncios_url, notice: 'Categoria anuncio was successfully destroyed.' }
      format.json { head :no_content }
    end
    create_log("Excluiu categoria de anuncio")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categoria_anuncio
      @categoria_anuncio = CategoriaAnuncio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def categoria_anuncio_params
      params.require(:categoria_anuncio).permit(:imagem, :retina_dimensions, :nome, :parent_id)
    end
end
