class CupomsController < ApplicationController
  before_action :set_cupom, only: [:show, :edit, :update, :destroy]

  # GET /cupoms
  # GET /cupoms.json
  def index
    @cupoms = Cupom.all
  end

  # GET /cupoms/1
  # GET /cupoms/1.json
  def show
  end

  # GET /cupoms/new
  def new
    @cupom = Cupom.new
  end

  # GET /cupoms/1/edit
  def edit
  end

  # POST /cupoms
  # POST /cupoms.json
  def create
    @cupom = Cupom.new(cupom_params)

    respond_to do |format|
      if @cupom.save
        format.html { redirect_to @cupom, notice: 'Cupom was successfully created.' }
        format.json { render :show, status: :created, location: @cupom }
      else
        format.html { render :new }
        format.json { render json: @cupom.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cupoms/1
  # PATCH/PUT /cupoms/1.json
  def update
    respond_to do |format|
      if @cupom.update(cupom_params)
        format.html { redirect_to @cupom, notice: 'Cupom was successfully updated.' }
        format.json { render :show, status: :ok, location: @cupom }
      else
        format.html { render :edit }
        format.json { render json: @cupom.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cupoms/1
  # DELETE /cupoms/1.json
  def destroy
    @cupom.destroy
    respond_to do |format|
      format.html { redirect_to cupoms_url, notice: 'Cupom was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cupom
      @cupom = Cupom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cupom_params
      params.require(:cupom).permit(:titulo, :subtitulo, :valor, :valor_desconto, :descricao, :quantidade_total, :quantidade_pessoa, :quantidade_dia, :regras, :utilizacao_inicio, :utilizacao_fim, :exibicao_inicio, :exibicao_fim, :hora_consumo_inicio, :hora_consumo_fim, :domingo, :segunda, :terca, :quarta, :quinta, :sexta, :sabado, :categoria_cupom_id, :usuario_id, :forma_pagamento_id, :forma_pagamento_id, :endereco_id)
    end
end
