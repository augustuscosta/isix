class CurtidaAnunciosController < ApplicationController
  before_action :set_curtida_anuncio, only: [:show, :edit, :update, :destroy]

  # GET /curtida_anuncios
  # GET /curtida_anuncios.json
  def index
    @curtida_anuncios = CurtidaAnuncio.all
  end

  # GET /curtida_anuncios/1
  # GET /curtida_anuncios/1.json
  def show
  end

  # GET /curtida_anuncios/new
  def new
    @curtida_anuncio = CurtidaAnuncio.new
  end

  # GET /curtida_anuncios/1/edit
  def edit
  end

  # POST /curtida_anuncios
  # POST /curtida_anuncios.json
  def create
    @curtida_anuncio = CurtidaAnuncio.new(curtida_anuncio_params)

    respond_to do |format|
      if @curtida_anuncio.save
        format.html { redirect_to @curtida_anuncio, notice: 'Curtida anuncio was successfully created.' }
        format.json { render :show, status: :created, location: @curtida_anuncio }
      else
        format.html { render :new }
        format.json { render json: @curtida_anuncio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /curtida_anuncios/1
  # PATCH/PUT /curtida_anuncios/1.json
  def update
    respond_to do |format|
      if @curtida_anuncio.update(curtida_anuncio_params)
        format.html { redirect_to @curtida_anuncio, notice: 'Curtida anuncio was successfully updated.' }
        format.json { render :show, status: :ok, location: @curtida_anuncio }
      else
        format.html { render :edit }
        format.json { render json: @curtida_anuncio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /curtida_anuncios/1
  # DELETE /curtida_anuncios/1.json
  def destroy
    @curtida_anuncio.destroy
    respond_to do |format|
      format.html { redirect_to curtida_anuncios_url, notice: 'Curtida anuncio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_curtida_anuncio
      @curtida_anuncio = CurtidaAnuncio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def curtida_anuncio_params
      params.require(:curtida_anuncio).permit(:anuncio_id, :usuario_id)
    end
end
