class AdesaosController < ApplicationController
  before_action :set_adesao, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!

  # GET /adesaos
  # GET /adesaos.json
  def index
    @adesaos = Adesao.all
  end

  # GET /adesaos/1
  # GET /adesaos/1.json
  def show
  end

  # GET /adesaos/new
  def new
    @adesao = Adesao.new
  end

  # GET /adesaos/1/edit
  def edit
  end

  # POST /adesaos
  # POST /adesaos.json
  def create
    @adesao = Adesao.new(adesao_params)

    respond_to do |format|
      if @adesao.save
        format.html { redirect_to @adesao, notice: 'Adesao was successfully created.' }
        format.json { render :show, status: :created, location: @adesao }
      else
        format.html { render :new }
        format.json { render json: @adesao.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adesaos/1
  # PATCH/PUT /adesaos/1.json
  def update
    respond_to do |format|
      if @adesao.update(adesao_params)
        format.html { redirect_to @adesao, notice: 'Adesao was successfully updated.' }
        format.json { render :show, status: :ok, location: @adesao }
      else
        format.html { render :edit }
        format.json { render json: @adesao.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adesaos/1
  # DELETE /adesaos/1.json
  def destroy
    @adesao.destroy
    respond_to do |format|
      format.html { redirect_to adesaos_url, notice: 'Adesao was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adesao
      @adesao = Adesao.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adesao_params
      params.require(:adesao).permit(:data, :valor, :validade, :usuario_id, :tipo_usuario_id)
    end
end
