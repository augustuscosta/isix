class FormaPagamentosController < ApplicationController
  before_action :set_forma_pagamento, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!
  before_action :is_marketing

  # GET /forma_pagamentos
  # GET /forma_pagamentos.json
  def index
    @forma_pagamentos = FormaPagamento.all
    create_log("Listar formas de pagamento")
  end

  # GET /forma_pagamentos/1
  # GET /forma_pagamentos/1.json
  def show
    create_log("Visualizar forma de pagamento")
  end

  # GET /forma_pagamentos/new
  def new
    @forma_pagamento = FormaPagamento.new
    create_log("Formulário de criação de nova forma de pagamento")
  end

  # GET /forma_pagamentos/1/edit
  def edit
    create_log("Formulário de edição da forma de pagamento")
  end

  # POST /forma_pagamentos
  # POST /forma_pagamentos.json
  def create
    @forma_pagamento = FormaPagamento.new(forma_pagamento_params)

    respond_to do |format|
      if @forma_pagamento.save
        format.html { redirect_to @forma_pagamento, notice: 'Forma pagamento was successfully created.' }
        format.json { render :show, status: :created, location: @forma_pagamento }
      else
        format.html { render :new }
        format.json { render json: @forma_pagamento.errors, status: :unprocessable_entity }
      end
    end
      create_log("Criou nova forma de pagamento")
  end

  # PATCH/PUT /forma_pagamentos/1
  # PATCH/PUT /forma_pagamentos/1.json
  def update
    respond_to do |format|
      if @forma_pagamento.update(forma_pagamento_params)
        format.html { redirect_to @forma_pagamento, notice: 'Forma pagamento was successfully updated.' }
        format.json { render :show, status: :ok, location: @forma_pagamento }
      else
        format.html { render :edit }
        format.json { render json: @forma_pagamento.errors, status: :unprocessable_entity }
      end
    end
    create_log("Editou forma de pagamento")
  end

  # DELETE /forma_pagamentos/1
  # DELETE /forma_pagamentos/1.json
  def destroy
    @forma_pagamento.destroy
    respond_to do |format|
      format.html { redirect_to forma_pagamentos_url, notice: 'Forma pagamento was successfully destroyed.' }
      format.json { head :no_content }
    end
    create_log("Excluiu forma de pagamento")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forma_pagamento
      @forma_pagamento = FormaPagamento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forma_pagamento_params
      params.require(:forma_pagamento).permit(:imagem, :retina_dimensions, :nome)
    end
end
