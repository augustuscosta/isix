class MensagemAnunciosController < ApplicationController
  before_action :set_mensagem_anuncio, only: [:show, :edit, :update, :destroy]

  # GET /mensagem_anuncios
  # GET /mensagem_anuncios.json
  def index
    @mensagem_anuncios = MensagemAnuncio.all
  end

  # GET /mensagem_anuncios/1
  # GET /mensagem_anuncios/1.json
  def show
  end

  # GET /mensagem_anuncios/new
  def new
    @mensagem_anuncio = MensagemAnuncio.new
  end

  # GET /mensagem_anuncios/1/edit
  def edit
  end

  # POST /mensagem_anuncios
  # POST /mensagem_anuncios.json
  def create
    @mensagem_anuncio = MensagemAnuncio.new(mensagem_anuncio_params)

    respond_to do |format|
      if @mensagem_anuncio.save
        format.html { redirect_to @mensagem_anuncio, notice: 'Mensagem anuncio was successfully created.' }
        format.json { render :show, status: :created, location: @mensagem_anuncio }
      else
        format.html { render :new }
        format.json { render json: @mensagem_anuncio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mensagem_anuncios/1
  # PATCH/PUT /mensagem_anuncios/1.json
  def update
    respond_to do |format|
      if @mensagem_anuncio.update(mensagem_anuncio_params)
        format.html { redirect_to @mensagem_anuncio, notice: 'Mensagem anuncio was successfully updated.' }
        format.json { render :show, status: :ok, location: @mensagem_anuncio }
      else
        format.html { render :edit }
        format.json { render json: @mensagem_anuncio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mensagem_anuncios/1
  # DELETE /mensagem_anuncios/1.json
  def destroy
    @mensagem_anuncio.destroy
    respond_to do |format|
      format.html { redirect_to mensagem_anuncios_url, notice: 'Mensagem anuncio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mensagem_anuncio
      @mensagem_anuncio = MensagemAnuncio.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mensagem_anuncio_params
      params.require(:mensagem_anuncio).permit(:mensagem, :anuncio_id, :usuario_id)
    end
end
