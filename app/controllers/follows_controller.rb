class FollowsController < ApplicationController
  before_action :authenticate_usuario!
  respond_to :js

  def create
    @usuario = Usuario.find(params[:usuario_id])
    current_usuario.follow(@usuario)
  end

  def destroy
    @usuario = Usuario.find(params[:usuario_id])
    current_usuario.stop_following(@usuario)
  end
end