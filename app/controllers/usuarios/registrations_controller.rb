class Usuarios::RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:usuario).permit(:nome, :tipo_usuario_id, :email, :password, :password_confirmation)
  end
end