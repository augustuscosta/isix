class Usuarios::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
   def new
    super
    puts '==============   new controller ================='  
    omniauth = request.env["omniauth.auth"]
    puts omniauth.inspect

   end

  # POST /resource/sign_in
  def create
    super
    puts '=================== SessionsController=================='
    omniauth = request.env["omniauth.auth"]
    puts omniauth.inspect

    if omniauth
      usuario = Usuario.find_by_provider_and_uid(omniauth['provider'], omniauth['uid'])
      puts '===== omniauth existe, usuario: =========='
      puts usuario.inspect
      if usuario
        flash[:notice] = "Signed in successfully."
        sign_in_and_redirect(:user, usuario)
      #Já logado
      elsif current_user
        usuario.update_attributes!(:provider => omniauth['provider'], :uid => omniauth['uid'])
        flash[:notice] = "Authentication successful."
        redirect_to root_url
      else
        puts '=== novo usuario ======'
        usuario = Usuario.new
        usuario.email = omniauth['user_info']['email'] if usuario.email.blank?
        if usuario.save
          flash[:notice] = "Signed in successfully."
          sign_in_and_redirect(:usuario, usuario)
        else
          session[:omniauth] = omniauth.except('extra')
          redirect_to new_usuario_registration_url
        end
      end
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
