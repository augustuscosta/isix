class CategoriaCupomsController < ApplicationController
  before_action :set_categoria_cupom, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_usuario!, except: [:index, :show]
  before_action :is_marketing, except: [:index, :show]

  # GET /categoria_cupoms
  # GET /categoria_cupoms.json
  def index
    @categoria_cupoms = CategoriaCupom.all
    create_log("Listar categorias de cupom")
  end

  # GET /categoria_cupoms/1
  # GET /categoria_cupoms/1.json
  def show
    create_log("Visualizar categoria de cupom")
  end

  # GET /categoria_cupoms/new
  def new
    @categoria_cupom = CategoriaCupom.new
    create_log("Formulário de criação de nova categoria de cupom")
  end

  # GET /categoria_cupoms/1/edit
  def edit
    create_log("Formulário de edição categoria de cupom")
  end

  # POST /categoria_cupoms
  # POST /categoria_cupoms.json
  def create
    @categoria_cupom = CategoriaCupom.new(categoria_cupom_params)

    respond_to do |format|
      if @categoria_cupom.save
        format.html { redirect_to @categoria_cupom, notice: 'Categoria cupom was successfully created.' }
        format.json { render :show, status: :created, location: @categoria_cupom }
      else
        format.html { render :new }
        format.json { render json: @categoria_cupom.errors, status: :unprocessable_entity }
      end
    end
    create_log("Criou nova categoria de cupom")
  end

  # PATCH/PUT /categoria_cupoms/1
  # PATCH/PUT /categoria_cupoms/1.json
  def update
    respond_to do |format|
      if @categoria_cupom.update(categoria_cupom_params)
        format.html { redirect_to @categoria_cupom, notice: 'Categoria cupom was successfully updated.' }
        format.json { render :show, status: :ok, location: @categoria_cupom }
      else
        format.html { render :edit }
        format.json { render json: @categoria_cupom.errors, status: :unprocessable_entity }
      end
    end
    create_log("Editou categoria de cupom")
  end

  # DELETE /categoria_cupoms/1
  # DELETE /categoria_cupoms/1.json
  def destroy
    @categoria_cupom.destroy
    respond_to do |format|
      format.html { redirect_to categoria_cupoms_url, notice: 'Categoria cupom was successfully destroyed.' }
      format.json { head :no_content }
    end
    create_log("Excluiu categoria de cupom")
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categoria_cupom
      @categoria_cupom = CategoriaCupom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def categoria_cupom_params
      params.require(:categoria_cupom).permit(:imagem, :retina_dimensions, :nome, :parent_id)
    end
end
