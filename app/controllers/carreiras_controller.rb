class CarreirasController < ApplicationController
  before_action :set_carreira, only: [:show, :edit, :update, :destroy]

  # GET /carreiras
  # GET /carreiras.json
  def index
    @carreiras = Carreira.all
  end

  # GET /carreiras/1
  # GET /carreiras/1.json
  def show
  end

  # GET /carreiras/new
  def new
    @carreira = Carreira.new
  end

  # GET /carreiras/1/edit
  def edit
  end

  # POST /carreiras
  # POST /carreiras.json
  def create
    @carreira = Carreira.new(carreira_params)

    respond_to do |format|
      if @carreira.save
        format.html { redirect_to @carreira, notice: 'Carreira was successfully created.' }
        format.json { render :show, status: :created, location: @carreira }
      else
        format.html { render :new }
        format.json { render json: @carreira.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /carreiras/1
  # PATCH/PUT /carreiras/1.json
  def update
    respond_to do |format|
      if @carreira.update(carreira_params)
        format.html { redirect_to @carreira, notice: 'Carreira was successfully updated.' }
        format.json { render :show, status: :ok, location: @carreira }
      else
        format.html { render :edit }
        format.json { render json: @carreira.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /carreiras/1
  # DELETE /carreiras/1.json
  def destroy
    @carreira.destroy
    respond_to do |format|
      format.html { redirect_to carreiras_url, notice: 'Carreira was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_carreira
      @carreira = Carreira.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def carreira_params
      params.require(:carreira).permit(:nivel1, :nivel2, :nivel3, :nivel4, :nivel5, :nivel6, :nivel7, :nivel8, :nivel9, :nivel10, :usuario_id)
    end
end
