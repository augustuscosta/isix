class UsuariosController < ApplicationController
  before_action :set_usuario, only: [:edit, :update, :destroy]
  before_action :authenticate_usuario!, except: [:show]
  before_action :is_admin_master, except: [:show]

  # GET /usuarios
  # GET /usuarios.json
  def index
    @usuarios = Usuario.all
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
    @usuario = Usuario.find(params[:id])
    @comentarios = @usuario.negocio.comments
    @horarios_funcionamento = @usuario.negocio.horario_funcionamento.to_list if @usuario.negocio.horario_funcionamento
    @telefones = []
    @telefones << {:tipo => 'whatsapp', :numero => @usuario.negocio.whatsapp } if @usuario.negocio && !@usuario.negocio.whatsapp.blank?
    @telefones << {:tipo => 'telefone', :numero => @usuario.negocio.telefone} if @usuario.negocio && !@usuario.negocio.telefone.blank?
    @telefones << {:tipo => 'celular', :numero => @usuario.negocio.celular} if @usuario.negocio && !@usuario.negocio.celular.blank?

    if current_usuario.blank?
      @meu_perfil = false
    else
      if @usuario.id === current_usuario.id
        @meu_perfil = true
      else
        @meu_perfil = false
      end
    end
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    @usuario = Usuario.new(usuario_params)

    respond_to do |format|
      if @usuario.save
        format.html { redirect_to @usuario, notice: 'Usuario was successfully created.' }
        format.json { render :show, status: :created, location: @usuario }
      else
        format.html { render :new }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    respond_to do |format|
      if @usuario.update(usuario_params)
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to usuarios_url, notice: 'Usuario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:imagem, :retina_dimensions, :nome, :usuario, :email, :password, :telefone, :sobre, :bloqueado, :mensagem_bloqueio, :endereco_id, :tipo_usuario_id, :grupo_id, :parent_id)
    end
end
