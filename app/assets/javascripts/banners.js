$(document).on('ready', function(event) {
  bannerReady();
});

$(document).on('ready page:load', function(event) {
  bannerReady();
});




$(document).ready(function () {bannerReady });


var getEstados = function() {
  var pai_id =  $("#banner_pai_id option:selected").val();
  if (pai_id == ""){
    $("#banner_cidade_id").empty();
    $("#banner_estado_id").empty();
    return;
  }
  $.ajax({
    url: "/pais/estados/"+pai_id,
    method: 'get',
    success: function(data){
      if(data){
        $("#banner_cidade_id").empty();
        $("#banner_estado_id").empty();
        for (var i in data){
          var id = data[i].id;
          var nome = data[i].nome;
          $("#banner_estado_id").append($('<option value="'+ id +'">'+ nome +'</option>'))
        }
      }
    },
    error: function(error){
      console.log(error);
    }
  });
}

var bannerReady = function() {
  //Recarrega o select de cidade após uma seleção do estado.
  $('#banner_pai_id').change(function() {
        getEstados();
  });



  //Recarrega o select de cidade após uma seleção do estado.
  $('#banner_estado_id').change(function() {
      var estado_id =  $("#banner_estado_id option:selected").val();
      if (estado_id == ""){
        $("#banner_cidade_id").empty();
        return;
      }
      $.ajax({
        url: "/estados/cidades/"+estado_id,
        method: 'get',
        success: function(data){
          if(data){
            $("#banner_cidade_id").empty();
            for (var i in data){
              var id = data[i].id;
              var nome = data[i].nome;
              $("#banner_cidade_id").append($('<option value="'+ id +'">'+ nome +'</option>'))
            }
          }
        },
        error: function(error){
          console.log(error);
        }
      });
    });



    if($( "#banner_pai_id" ).val() != null && $( "#banner_estado_id" ).val() == null){
      getEstados();
    }
};
