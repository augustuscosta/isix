task 'db:erase' do
  Rake::Task["db:drop"].reenable
  Rake::Task['db:drop'].invoke
  Rake::Task["db:create"].reenable
  Rake::Task['db:create'].invoke
  Rake::Task["db:migrate"].reenable
  Rake::Task['db:migrate'].invoke
end

task 'db:dev' => 'db:erase' do
  Rake::Task["db:seed"].reenable
  Rake::Task['db:seed'].invoke
end