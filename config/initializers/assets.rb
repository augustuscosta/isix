# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path
Rails.application.config.assets.paths << Rails.root.join("/app/assets/fonts")
Rails.application.config.assets.precompile += %w( img)
# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += %w( bootstrap.min.js )
Rails.application.config.assets.precompile += %w( countdown.min.js )
Rails.application.config.assets.precompile += %w( flexnav.min.js )
Rails.application.config.assets.precompile += %w( magnific.js )
Rails.application.config.assets.precompile += %w( tweet.min.js )
Rails.application.config.assets.precompile += %w( fitvids.min.js )
Rails.application.config.assets.precompile += %w( mail.min.js )
Rails.application.config.assets.precompile += %w( ionrangeslider.js )
Rails.application.config.assets.precompile += %w( icheck.js )
Rails.application.config.assets.precompile += %w( fotorama.js )
Rails.application.config.assets.precompile += %w( card-payment.js )
Rails.application.config.assets.precompile += %w( owl-carousel.js )
Rails.application.config.assets.precompile += %w( masonry.js )
Rails.application.config.assets.precompile += %w( nicescroll.js )
Rails.application.config.assets.precompile += %w( custom.js )
Rails.application.config.assets.precompile += %w( jquery.js )
Rails.application.config.assets.precompile += %w( bootstrap.css )
Rails.application.config.assets.precompile += %w( font_awesome.css )
Rails.application.config.assets.precompile += %w( styles.css )
Rails.application.config.assets.precompile += %w( mystyles.css )


