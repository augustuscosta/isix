Rails.application.routes.draw do
  devise_for :usuarios, :controllers => { :omniauth_callbacks => "usuarios/omniauth_callbacks", 
    :sessions => "usuarios/sessions", :registrations => 'usuarios/registrations' }
  
  resources :mensagem_anuncios
  resources :categoria_anuncios
  resources :curtida_anuncios
  resources :anuncios
  resources :forma_pagamentos
  resources :cupoms
  resources :mensagem_cupoms
  resources :categoria_cupoms
  resources :descontos
  resources :banners, except: [:show]
  resources :carreiras
  resources :adesaos
  resources :retiradas
  resources :logs, except: [:edit, :update, :show]
  resources :mensagems
  resources :cidades
  resources :estados
  resources :pais
  resources :enderecos
  resources :negocios
  resources :conta
  resources :grupos
  resources :tipo_usuarios
  resources :usuarios
  resources :comments, only: [:create, :destroy]

  root :to => 'categoria_anuncios#index'

  get '/pais/estados/:pai_id', to: 'pais#estados'
  get '/estados/cidades/:estado_id', to: 'estados#cidades'

  
  get '/auth/failure', to: redirect('/')

  devise_scope :usuario do
    get '/auth/facebook/callback', to: 'usuarios/sessions#create'

    #get 'sign_in', :to => 'devise/sessions#new', :as => :new_usuario_session
    get '/usuarios/sign_out', :to => 'devise/sessions#destroy'# :as => :destroy_usuario_session
  
  end

  match :like, to: 'likes#create', as: :like, via: :post
  match :unlike, to: 'likes#destroy', as: :unlike, via: :post
  match :follow, to: 'follows#create', as: :follow, via: :post
  match :unfollow, to: 'follows#destroy', as: :unfollow, via: :post

  #get 'signout', to: 'devise/sessions#destroy', as: 'signout'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
