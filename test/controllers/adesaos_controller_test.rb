require 'test_helper'

class AdesaosControllerTest < ActionController::TestCase
  setup do
    @adesao = adesaos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:adesaos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create adesao" do
    assert_difference('Adesao.count') do
      post :create, adesao: { data: @adesao.data, tipo_usuario_id: @adesao.tipo_usuario_id, usuario_id: @adesao.usuario_id, validade: @adesao.validade, valor: @adesao.valor }
    end

    assert_redirected_to adesao_path(assigns(:adesao))
  end

  test "should show adesao" do
    get :show, id: @adesao
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @adesao
    assert_response :success
  end

  test "should update adesao" do
    patch :update, id: @adesao, adesao: { data: @adesao.data, tipo_usuario_id: @adesao.tipo_usuario_id, usuario_id: @adesao.usuario_id, validade: @adesao.validade, valor: @adesao.valor }
    assert_redirected_to adesao_path(assigns(:adesao))
  end

  test "should destroy adesao" do
    assert_difference('Adesao.count', -1) do
      delete :destroy, id: @adesao
    end

    assert_redirected_to adesaos_path
  end
end
