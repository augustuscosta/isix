require 'test_helper'

class UsuariosControllerTest < ActionController::TestCase
  setup do
    @usuario = usuarios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:usuarios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create usuario" do
    assert_difference('Usuario.count') do
      post :create, usuario: { bloqueado: @usuario.bloqueado, email: @usuario.email, endereco_id: @usuario.endereco_id, grupo_id: @usuario.grupo_id, mensagem_bloqueio: @usuario.mensagem_bloqueio, nome: @usuario.nome, parent_id: @usuario.parent_id, password: @usuario.password, sobre: @usuario.sobre, telefone: @usuario.telefone, tipo_usuario_id: @usuario.tipo_usuario_id, usuario: @usuario.usuario }
    end

    assert_redirected_to usuario_path(assigns(:usuario))
  end

  test "should show usuario" do
    get :show, id: @usuario
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @usuario
    assert_response :success
  end

  test "should update usuario" do
    patch :update, id: @usuario, usuario: { bloqueado: @usuario.bloqueado, email: @usuario.email, endereco_id: @usuario.endereco_id, grupo_id: @usuario.grupo_id, mensagem_bloqueio: @usuario.mensagem_bloqueio, nome: @usuario.nome, parent_id: @usuario.parent_id, password: @usuario.password, sobre: @usuario.sobre, telefone: @usuario.telefone, tipo_usuario_id: @usuario.tipo_usuario_id, usuario: @usuario.usuario }
    assert_redirected_to usuario_path(assigns(:usuario))
  end

  test "should destroy usuario" do
    assert_difference('Usuario.count', -1) do
      delete :destroy, id: @usuario
    end

    assert_redirected_to usuarios_path
  end
end
