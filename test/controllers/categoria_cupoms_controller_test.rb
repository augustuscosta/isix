require 'test_helper'

class CategoriaCupomsControllerTest < ActionController::TestCase
  setup do
    @categoria_cupom = categoria_cupoms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:categoria_cupoms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create categoria_cupom" do
    assert_difference('CategoriaCupom.count') do
      post :create, categoria_cupom: { nome: @categoria_cupom.nome, parent_id: @categoria_cupom.parent_id }
    end

    assert_redirected_to categoria_cupom_path(assigns(:categoria_cupom))
  end

  test "should show categoria_cupom" do
    get :show, id: @categoria_cupom
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @categoria_cupom
    assert_response :success
  end

  test "should update categoria_cupom" do
    patch :update, id: @categoria_cupom, categoria_cupom: { nome: @categoria_cupom.nome, parent_id: @categoria_cupom.parent_id }
    assert_redirected_to categoria_cupom_path(assigns(:categoria_cupom))
  end

  test "should destroy categoria_cupom" do
    assert_difference('CategoriaCupom.count', -1) do
      delete :destroy, id: @categoria_cupom
    end

    assert_redirected_to categoria_cupoms_path
  end
end
