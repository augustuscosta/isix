require 'test_helper'

class CategoriaAnunciosControllerTest < ActionController::TestCase
  setup do
    @categoria_anuncio = categoria_anuncios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:categoria_anuncios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create categoria_anuncio" do
    assert_difference('CategoriaAnuncio.count') do
      post :create, categoria_anuncio: { nome: @categoria_anuncio.nome, parent_id: @categoria_anuncio.parent_id }
    end

    assert_redirected_to categoria_anuncio_path(assigns(:categoria_anuncio))
  end

  test "should show categoria_anuncio" do
    get :show, id: @categoria_anuncio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @categoria_anuncio
    assert_response :success
  end

  test "should update categoria_anuncio" do
    patch :update, id: @categoria_anuncio, categoria_anuncio: { nome: @categoria_anuncio.nome, parent_id: @categoria_anuncio.parent_id }
    assert_redirected_to categoria_anuncio_path(assigns(:categoria_anuncio))
  end

  test "should destroy categoria_anuncio" do
    assert_difference('CategoriaAnuncio.count', -1) do
      delete :destroy, id: @categoria_anuncio
    end

    assert_redirected_to categoria_anuncios_path
  end
end
