require 'test_helper'

class CurtidaAnunciosControllerTest < ActionController::TestCase
  setup do
    @curtida_anuncio = curtida_anuncios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:curtida_anuncios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create curtida_anuncio" do
    assert_difference('CurtidaAnuncio.count') do
      post :create, curtida_anuncio: { anuncio_id: @curtida_anuncio.anuncio_id, usuario_id: @curtida_anuncio.usuario_id }
    end

    assert_redirected_to curtida_anuncio_path(assigns(:curtida_anuncio))
  end

  test "should show curtida_anuncio" do
    get :show, id: @curtida_anuncio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @curtida_anuncio
    assert_response :success
  end

  test "should update curtida_anuncio" do
    patch :update, id: @curtida_anuncio, curtida_anuncio: { anuncio_id: @curtida_anuncio.anuncio_id, usuario_id: @curtida_anuncio.usuario_id }
    assert_redirected_to curtida_anuncio_path(assigns(:curtida_anuncio))
  end

  test "should destroy curtida_anuncio" do
    assert_difference('CurtidaAnuncio.count', -1) do
      delete :destroy, id: @curtida_anuncio
    end

    assert_redirected_to curtida_anuncios_path
  end
end
