require 'test_helper'

class CarreirasControllerTest < ActionController::TestCase
  setup do
    @carreira = carreiras(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:carreiras)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create carreira" do
    assert_difference('Carreira.count') do
      post :create, carreira: { nivel10: @carreira.nivel10, nivel1: @carreira.nivel1, nivel2: @carreira.nivel2, nivel3: @carreira.nivel3, nivel4: @carreira.nivel4, nivel5: @carreira.nivel5, nivel6: @carreira.nivel6, nivel7: @carreira.nivel7, nivel8: @carreira.nivel8, nivel9: @carreira.nivel9, usuario_id: @carreira.usuario_id }
    end

    assert_redirected_to carreira_path(assigns(:carreira))
  end

  test "should show carreira" do
    get :show, id: @carreira
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @carreira
    assert_response :success
  end

  test "should update carreira" do
    patch :update, id: @carreira, carreira: { nivel10: @carreira.nivel10, nivel1: @carreira.nivel1, nivel2: @carreira.nivel2, nivel3: @carreira.nivel3, nivel4: @carreira.nivel4, nivel5: @carreira.nivel5, nivel6: @carreira.nivel6, nivel7: @carreira.nivel7, nivel8: @carreira.nivel8, nivel9: @carreira.nivel9, usuario_id: @carreira.usuario_id }
    assert_redirected_to carreira_path(assigns(:carreira))
  end

  test "should destroy carreira" do
    assert_difference('Carreira.count', -1) do
      delete :destroy, id: @carreira
    end

    assert_redirected_to carreiras_path
  end
end
