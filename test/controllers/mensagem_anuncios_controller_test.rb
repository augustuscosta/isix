require 'test_helper'

class MensagemAnunciosControllerTest < ActionController::TestCase
  setup do
    @mensagem_anuncio = mensagem_anuncios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mensagem_anuncios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mensagem_anuncio" do
    assert_difference('MensagemAnuncio.count') do
      post :create, mensagem_anuncio: { anuncio_id: @mensagem_anuncio.anuncio_id, mensagem: @mensagem_anuncio.mensagem, usuario_id: @mensagem_anuncio.usuario_id }
    end

    assert_redirected_to mensagem_anuncio_path(assigns(:mensagem_anuncio))
  end

  test "should show mensagem_anuncio" do
    get :show, id: @mensagem_anuncio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mensagem_anuncio
    assert_response :success
  end

  test "should update mensagem_anuncio" do
    patch :update, id: @mensagem_anuncio, mensagem_anuncio: { anuncio_id: @mensagem_anuncio.anuncio_id, mensagem: @mensagem_anuncio.mensagem, usuario_id: @mensagem_anuncio.usuario_id }
    assert_redirected_to mensagem_anuncio_path(assigns(:mensagem_anuncio))
  end

  test "should destroy mensagem_anuncio" do
    assert_difference('MensagemAnuncio.count', -1) do
      delete :destroy, id: @mensagem_anuncio
    end

    assert_redirected_to mensagem_anuncios_path
  end
end
