require 'test_helper'

class CupomsControllerTest < ActionController::TestCase
  setup do
    @cupom = cupoms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cupoms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cupom" do
    assert_difference('Cupom.count') do
      post :create, cupom: { categoria_cupom_id: @cupom.categoria_cupom_id, descricao: @cupom.descricao, domingo: @cupom.domingo, endereco_id: @cupom.endereco_id, exibicao_fim: @cupom.exibicao_fim, exibicao_inicio: @cupom.exibicao_inicio, forma_pagamento_id: @cupom.forma_pagamento_id, forma_pagamento_id: @cupom.forma_pagamento_id, hora_consumo_fim: @cupom.hora_consumo_fim, hora_consumo_inicio: @cupom.hora_consumo_inicio, quantidade_dia: @cupom.quantidade_dia, quantidade_pessoa: @cupom.quantidade_pessoa, quantidade_total: @cupom.quantidade_total, quarta: @cupom.quarta, quinta: @cupom.quinta, regras: @cupom.regras, sabado: @cupom.sabado, segunda: @cupom.segunda, sexta: @cupom.sexta, subtitulo: @cupom.subtitulo, terca: @cupom.terca, titulo: @cupom.titulo, usuario_id: @cupom.usuario_id, utilizacao_fim: @cupom.utilizacao_fim, utilizacao_inicio: @cupom.utilizacao_inicio, valor: @cupom.valor, valor_desconto: @cupom.valor_desconto }
    end

    assert_redirected_to cupom_path(assigns(:cupom))
  end

  test "should show cupom" do
    get :show, id: @cupom
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cupom
    assert_response :success
  end

  test "should update cupom" do
    patch :update, id: @cupom, cupom: { categoria_cupom_id: @cupom.categoria_cupom_id, descricao: @cupom.descricao, domingo: @cupom.domingo, endereco_id: @cupom.endereco_id, exibicao_fim: @cupom.exibicao_fim, exibicao_inicio: @cupom.exibicao_inicio, forma_pagamento_id: @cupom.forma_pagamento_id, forma_pagamento_id: @cupom.forma_pagamento_id, hora_consumo_fim: @cupom.hora_consumo_fim, hora_consumo_inicio: @cupom.hora_consumo_inicio, quantidade_dia: @cupom.quantidade_dia, quantidade_pessoa: @cupom.quantidade_pessoa, quantidade_total: @cupom.quantidade_total, quarta: @cupom.quarta, quinta: @cupom.quinta, regras: @cupom.regras, sabado: @cupom.sabado, segunda: @cupom.segunda, sexta: @cupom.sexta, subtitulo: @cupom.subtitulo, terca: @cupom.terca, titulo: @cupom.titulo, usuario_id: @cupom.usuario_id, utilizacao_fim: @cupom.utilizacao_fim, utilizacao_inicio: @cupom.utilizacao_inicio, valor: @cupom.valor, valor_desconto: @cupom.valor_desconto }
    assert_redirected_to cupom_path(assigns(:cupom))
  end

  test "should destroy cupom" do
    assert_difference('Cupom.count', -1) do
      delete :destroy, id: @cupom
    end

    assert_redirected_to cupoms_path
  end
end
