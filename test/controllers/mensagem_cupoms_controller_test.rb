require 'test_helper'

class MensagemCupomsControllerTest < ActionController::TestCase
  setup do
    @mensagem_cupom = mensagem_cupoms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mensagem_cupoms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mensagem_cupom" do
    assert_difference('MensagemCupom.count') do
      post :create, mensagem_cupom: { cupom_id: @mensagem_cupom.cupom_id, mensagem: @mensagem_cupom.mensagem, usuario_id: @mensagem_cupom.usuario_id }
    end

    assert_redirected_to mensagem_cupom_path(assigns(:mensagem_cupom))
  end

  test "should show mensagem_cupom" do
    get :show, id: @mensagem_cupom
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mensagem_cupom
    assert_response :success
  end

  test "should update mensagem_cupom" do
    patch :update, id: @mensagem_cupom, mensagem_cupom: { cupom_id: @mensagem_cupom.cupom_id, mensagem: @mensagem_cupom.mensagem, usuario_id: @mensagem_cupom.usuario_id }
    assert_redirected_to mensagem_cupom_path(assigns(:mensagem_cupom))
  end

  test "should destroy mensagem_cupom" do
    assert_difference('MensagemCupom.count', -1) do
      delete :destroy, id: @mensagem_cupom
    end

    assert_redirected_to mensagem_cupoms_path
  end
end
